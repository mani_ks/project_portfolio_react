import './Message.css'
import quote from '../../assets/left-quote.png';

const Message = function () {
    return (
        <div className="mt-5 mb-5">
            <div className="container"> 
                <div className="row">
                    <div className="p-4">
                        <div className="msg-card p-4 pt-5">
                            <div className="quote-container">
                                <img src={quote} alt="quote" width="100%" height="100%" />
                            </div>
                            <p className="mt-3">
                                Kumar Engineering Co. (KEC) was conceived with the aim to break the dependency on Europe and China for <u>woodworking machines</u> and make India self reliant in this domain.
                            </p>
                            <p className="mt-4">
                                At KEC, we have combined our 20+ years of acquired skills in this area, in-depth understanding of the industry, and knowledge of <u>variations in the processed woods</u> while incorporating the latest features on the machines available in the market.
                            </p>
                            <p className="mt-4">
                                Our machines are offered at a highly competitive price and backed by a strong after-sales service, which has resulted in a highly satisfied customer base nationwide.
                            </p>
                            <p className="mt-4">
                                Encouraged by our achievements we are now raising the bar further and are poised to take our business to a wider international market and also expand our product portfolio.
                            </p>
                        </div>
                    </div>
                </div>
             </div>
        </div>
    )
}

export default Message
import React from 'react';
import kecLogo from '../../assets/KEC_logo.png'
import './Topbar.css'

const Toolbar = ()=>{
    const containerStyleObj = {
        minHeight: '10px'
    };

    return (
        <nav className="navBar">
            <div className="container" style={containerStyleObj}>
                <div className="d-flex align-items-center justify-content-between">
                    <div className="pt-2 pb-2">
                        <a href="https://kecindia.org/" style={{border: 'none'}} target="_blank" rel="noreferrer">
                            <img src={kecLogo} alt="kec_logo" height="40"></img>
                        </a>
                    </div>
                    {/* <div className="line"></div>
                    <p className="m-0 name">Sunil Srivastava</p> */}
                    <a href="https://drive.google.com/drive/folders/11CiiZ6NIUJ6GykFWXfKBZexgHxH09PFf?usp=sharing">
                        <div className="catalogueButton p-2">
                            <span>Catalogue</span>
                        </div>
                    </a>
                </div>
            </div>
        </nav>
    )
}

export default Toolbar
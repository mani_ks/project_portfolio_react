import './Links.css';

const images = require.context('../../assets', true);

const contactUs = [
    {
        key: "tollfree",
        title: '+91-8826298005',
        href: 'tel:+918826298005',
        class: 'mt-3'
    },
    {
        key: "email1",
        title: 'info@kecindia.org',
        href: 'mailto:info@kecindia.org',
        class: 'mt-4'
    },
    {
        key: "email2",
        title: 'marketing@kumarengineeringco.in',
        href: 'mailto:marketing@kumarengineeringco.in',
        class: 'mt-2'
    }
] 

const followUs = [
    {
        key: 'youtube',
        location: './youtube.png',
        href: 'https://www.youtube.com/channel/UCoHAKeeJOi4jK8Pw4Do7GrQ'
    },
    {
        key: 'facebook',
        location: './facebook.png',
        href: 'https://www.facebook.com/kumarEngCo'
    },
    {
        key: 'linkedin',
        location: './linkedin.png',
        href: 'https://www.linkedin.com/company/kumar-engg-co/'
    },
    {
        key: 'website',
        location: './world-wide-web.png',
        href: 'https://kecindia.org/'
    }
]

function Links() {
    return (
        <div className="container">
            <div className="row">
                <div className="col-12 col-md-5 d-flex justify-content-center justify-content-md-start p-4">
                    <div className="contact-card text-center text-md-start">
                        <h3>Follow Us</h3>
                        <div className="d-flex align-items-center justify-content-center justify-content-md-start">
                            {
                                followUs.map((item)=>{
                                    return (
                                        <div className="mt-2 mb-2" key={item.key} style={{marginRight: '20px'}}>
                                            <a href={item.href} target="_blank" rel="noreferrer">
                                                <img src={images(item.location)} alt={item.key} width="30" height="30"></img>
                                            </a>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
                <div className="col-12 col-md-7 d-flex justify-content-center justify-content-md-end p-4">
                    <div className="contact-card text-center text-md-end">
                        <h3>Contact Us</h3>
                        {
                            contactUs.map((item)=>{
                                return (
                                    <div key={item.key} className={item.class}>
                                        <a href={item.href}>
                                            <span>{item.title}</span>
                                        </a>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Links;
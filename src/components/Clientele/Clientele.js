import './Clientele.css'
import { clienteleData } from './ClienteleData'

const images = require.context('../../assets/clients', true);

function Clientele() {
    return (
        <div className="mt-5 mb-5">
            <div className="container"> 
                <div className="row">
                    <div className="p-4">
                        <div className="clientele-card p-3">
                            <div className="pt-4 mb-5 text-center">
                                <h4>KEC's Clientele</h4>
                            </div>
                            <div className="p-4 pb-5" style={{maxHeight: '400px', overflowY: 'scroll'}}>
                                <div className="row flex-wrap">
                                    {
                                        clienteleData.map((item)=>{
                                            return (
                                                <div key={item.id} className="col-6 col-sm-3 col-lg-2 d-flex align-item-center justify-content-center">
                                                    <a key={item.id} className="d-flex align-items-center justify-content-center" href={item.websitePath} target="_blank" rel="noreferrer">
                                                        <div className="p-2 logo-card d-flex align-items-center">
                                                            <img src={images(item.imageSource)} alt={item.id} width="100%" />
                                                        </div>
                                                    </a>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Clientele;
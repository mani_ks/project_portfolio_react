export const clienteleData = [
    {
        id: '1_Century',
        imageSource: './1_Century.png',
        websitePath: 'https://www.centuryply.com/'
    },
    {
        id: '2_Merino',
        imageSource: './2_Merino.jpg',
        websitePath: 'http://www.merinolaminates.com/'
    },
    {
        id: '3_Green',
        imageSource: './3_Green.png',
        websitePath: 'https://www.greenply.com/'
    },
    {
        id: '4_Duro',
        imageSource: './4_Duro.png',
        websitePath: 'https://www.duroply.in/'
    },
    {
        id: '5_Joy_Bhavya',
        imageSource: './5_Joy_Bhavya.png',
        websitePath: 'https://www.joyplyboard.com/'
    },
    {
        id: '6_Signature',
        imageSource: './6_Signature.png',
        websitePath: 'http://www.signaturelam.com/'
    },
    {
        id: '7_Virgo',
        imageSource: './7_Virgo.png',
        websitePath: 'https://www.virgolam.com/'
    },
    {
        id: '8_Globe_Panel',
        imageSource: './8_Globe_Panel.png',
        websitePath: 'http://www.globepanel.in/'
    },
    {
        id: '9_Rajdhani',
        imageSource: './9_Rajdhani.png',
        websitePath: 'https://www.rajdhanicrafts.com/'
    },
    {
        id: '10_Hilwud',
        imageSource: './10_Hilwud.jpg',
        websitePath: '#'
    },
    {
        id: '11_Gattani_Ply.jpg',
        imageSource: './11_Gattani_Ply.jpg',
        websitePath: 'https://www.gattaniplywood.com/'
    },
    {
        id: '12_GMG.png',
        imageSource: './12_GMG.png',
        websitePath: 'http://www.gmgplywoods.com/'
    },
    {
        id: '13_Kuldeep_Plywood',
        imageSource: './13_Kuldeep_Plywood.jpg',
        websitePath: 'https://www.kuldeepplywoodindustries.com/'
    },
    {
        id: '14_Shree_Gopal_Udyog',
        imageSource: './14_Shree_Gopal_Udyog.png',
        websitePath: 'http://www.sguply.com/'
    },
    {
        id: '15_Solid_Ply',
        imageSource: './15_Solid_Ply.jpg',
        websitePath: 'http://www.solidply.com/'
    },
    {
        id: '16_Tirupati_Plywood',
        imageSource: './16_Tirupati_Plywood.jpg',
        websitePath: 'https://tirupati-plywood-industries.business.site/'
    },
    {
        id: '17_API_Ply_Alakhnanda',
        imageSource: './17_API_Ply_Alakhnanda.jpeg',
        websitePath: 'http://www.apiplyboard.com/'
    },
    {
        id: '18_New_Pragati',
        imageSource: './18_New_Pragati.jpeg',
        websitePath: 'http://www.nppiply.com/'
    },
    {
        id: '19_Interwood_Vidhata',
        imageSource: './19_Interwood_Vidhata.png',
        websitePath: 'http://interwoodplus.com/'
    },
    {
        id: '20_Bankey_Bihari_Plywood',
        imageSource: './20_Bankey_Bihari_Plywood.png',
        websitePath: 'https://www.kanpurplywood.com/'
    },
    {
        id: '21_Central_India',
        imageSource: './21_Central_India.jpg',
        websitePath: 'http://www.centralindiaveneers.com/'
    },
    {
        id: '22_Jagdamba_Wood',
        imageSource: './22_Jagdamba_Wood.png',
        websitePath: 'http://www.jagdambawood.in/'
    },
    {
        id: '23_Keshav_Ply',
        imageSource: './23_Keshav_Ply.jpeg',
        websitePath: 'http://kpigroups.org/'
    },
    {
        id: '24_Koshika_Ply',
        imageSource: './24_Koshika_Ply.png',
        websitePath: 'http://www.koshikaplywood.com/'
    },
    {
        id: '25_Neply',
        imageSource: './25_Neply.jpg',
        websitePath: 'https://www.facebook.com/NeplyOfficial/'
    },
    {
        id: '26_Samrat',
        imageSource: './26_Samrat.jpeg',
        websitePath: 'https://samratply.in/'
    },
    {
        id: '27_RP_Wood',
        imageSource: './27_RP_Wood.png',
        websitePath: 'http://rpwood.in/'
    },

];


import './Content.css';

const images = require.context('../../assets', true);

const logoArr = [
    {
        key: 'phone',
        location: './phone.png',
        href: 'tel:+918222931401'
    },
    {
        key: 'whatsapp',
        location: './whatsapp.png',
        href: 'https://wa.me/918222931401'
    },
    {
        key: 'mail',
        location: './gmail.png',
        href: 'mailto:sunil@kumarengineeringco.in'
    },
    {
        key: 'linkedin',
        location: './linkedin.png',
        href: 'https://www.linkedin.com/in/sunil-kumar-srivastava-10189876/'
    }
]

const Content = function() {
    return (
        <div className="main-card-cover">
            <div className="main-card-overlay pt-5 pb-5">
                <div className="container">
                    <div className="d-flex align-items-center justify-content-center">
                        <div className="card-container d-md-inline-flex justify-content-md-between pt-5 pb-5 p-sm-5">
                            <div className="img-container d-flex align-items-center justify-content-center">
                                <div className="imageDiv"></div>
                            </div>
                            <div className="data-container mt-5 mt-md-0 d-flex flex-column align-items-center justify-content-center">
                                <h1>Sunil Srivastava</h1>
                                <h6>Founder & Managing Director</h6>
                                <p>Kumar Engineering Co.</p>
                                <div className="d-flex align-items-center justify-content-center">
                                    {
                                        logoArr.map((item)=>{
                                            return (
                                                <div className="m-2" key={item.key}>
                                                    <a href={item.href} target="_blank" rel="noreferrer">
                                                        <img src={images(item.location)} alt={item.key} width="25" height="25"></img>
                                                    </a>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Content;
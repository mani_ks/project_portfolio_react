const Footer = function() {
    return (
        <div className="d-flex justify-content-center" style={{color: 'white', backgroundColor: '#6c6c6c'}}>
            <p className="m-0 p-3 text-center">Designed and Developed by <br /><a href="https://www.linkedin.com/in/ksmani93/" style={{textDecoration: 'none'}}><code style={{color: 'white'}}>Mani Kumar Srivastava</code></a></p>
        </div>
    )
}

export default Footer
import React from 'react';
import Topbar from './components/Topbar/Topbar'
import Content from './components/Content/Content'
import './App.css';
import Message from './components/Message/Message';
import Links from './components/Links/Links';
import Footer from './components/Footer/Footer';
import Clientele from './components/Clientele/Clientele';

function App() {

  return (
    <React.Fragment>
      <Topbar ></Topbar>
      <Content></Content>
      <Message></Message>
      <Clientele></Clientele>
      <Links></Links>
      <Footer></Footer>
    </React.Fragment>    
  );
}

export default App;
